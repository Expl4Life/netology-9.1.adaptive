$(document).ready(function(){
	// Выпадающее меню для планшетов и мобильных телефонов.
var touch = $('#touch-menu');
var menu = $('.links');
$(touch).on('click', function(e){
    e.preventDefault();
    menu.slideToggle();
});
$(window).resize(function(){
    var wid = $(window).width();
    if (wid>640 && menu.is(':hidden')) {
        menu.removeAttr('style');
    }
    if(wid<640) {
         menu.css({display: 'none'})
    }
});

});